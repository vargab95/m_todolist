#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <m_libs/m_list.h>
#include <m_libs/m_map.h>
#include <m_libs/m_common.h>

typedef enum {
    M_TODOLIST_OK = 0,
    M_TODOLIST_FILE_ERROR = 1,
    M_TODOLIST_EXIT = 2
} m_todolist_error_code_t;

// Interface functions
m_todolist_error_code_t print_help(m_list_t **list, m_com_sized_data_t *buffer);
m_todolist_error_code_t load_database(m_list_t **list, m_com_sized_data_t *buffer);
m_todolist_error_code_t dump_database(m_list_t **list, m_com_sized_data_t *buffer);
m_todolist_error_code_t add_todo(m_list_t **list, m_com_sized_data_t *buffer);
m_todolist_error_code_t delete_todo(m_list_t **list, m_com_sized_data_t *buffer);
m_todolist_error_code_t list_todos(m_list_t **list, m_com_sized_data_t *buffer);
m_todolist_error_code_t exit_todolist(m_list_t **list, m_com_sized_data_t *buffer);

// Internal functions
void add_command_to_map(m_map_t *map, char *command, m_todolist_error_code_t (*function)(m_list_t **list, m_com_sized_data_t *buffer));

int main(int argc, char **argv)
{
    m_list_t *todolist = NULL;
    m_map_t *command_map = m_map_create(10);
    m_com_sized_data_t input_data, command, *function;
    char data_buffer[1024];
    char command_buffer[128];
    uint8_t exit_called = 0;

    input_data.data = data_buffer;
    command.data = command_buffer;

    if (argc > 1)
    {
        m_com_sized_data_t tmp = {argv[1]};
        load_database(&todolist, &tmp);
    }

    if (!todolist)
    {
        todolist = m_list_create();
    }

    add_command_to_map(command_map, "help", print_help);
    add_command_to_map(command_map, "load", load_database);
    add_command_to_map(command_map, "dump", dump_database);
    add_command_to_map(command_map, "add", add_todo);
    add_command_to_map(command_map, "delete", delete_todo);
    add_command_to_map(command_map, "list", list_todos);
    add_command_to_map(command_map, "exit", exit_todolist);

    while(!exit_called)
    {
        m_todolist_error_code_t error_code;
        m_todolist_error_code_t (*function_ptr)(m_list_t **list, m_com_sized_data_t *buffer);

        if (isatty(0))
        {
            printf("> ");
        }

        if (scanf("%127s", command_buffer) != 1)
        {
            puts("Failed to read command");
            continue;
        }

        command.size = strlen(command_buffer);

        if (getchar() != '\n')
        {
            if (scanf("%1023[^\n]", data_buffer) != 1)
            {
                puts("Failed to read argument");
                continue;
            }
            input_data.size = strlen(data_buffer);
            while (getchar() != '\n');
        }

        function = m_map_get(command_map, &command);
        if (!function)
        {
            printf("%s is not a valid command\n", (char*)command.data);
            continue;
        }

        function_ptr = function->data;
        error_code = function_ptr(&todolist, &input_data);
        switch (error_code)
        {
            case M_TODOLIST_OK:
                break;
            case M_TODOLIST_EXIT:
                exit_called = 1;
                break;
            case M_TODOLIST_FILE_ERROR:
                puts("File error");
                break;
            default:
                puts("Unhandled error code");
                break;
        }
    }

    if (argc > 1)
    {
        m_com_sized_data_t tmp = {argv[1]};
        dump_database(&todolist, &tmp);
    }

    m_list_destroy(&todolist);
    m_map_destroy(&command_map);

    return 0;
}


m_todolist_error_code_t print_help(m_list_t **list, m_com_sized_data_t *buffer)
{
    puts("help   - Prints this message");
    puts("load   - Loads the database from a file");
    puts("dump   - Dumps the database to a file");
    puts("add    - Adds a todo to the in memory database");
    puts("delete - Deletes a todo from the in memory database");
    puts("list   - Lists all todos in the in memory database");
    return M_TODOLIST_OK;
}


m_todolist_error_code_t load_database(m_list_t **list, m_com_sized_data_t *buffer)
{
    FILE *database_file = fopen(buffer->data, "r");
    if (!database_file)
    {
        fprintf(stderr, "Cannot open %s\n", (char*)buffer->data);
        return M_TODOLIST_FILE_ERROR;
    }

    *list = m_list_load_binary(database_file);
    fclose(database_file);

    return M_TODOLIST_OK;
}


m_todolist_error_code_t dump_database(m_list_t **list, m_com_sized_data_t *buffer)
{
    FILE *database_file = fopen(buffer->data, "w");
    if (!database_file)
    {
        fprintf(stderr, "Cannot open %s\n", (char*)buffer->data);
        return M_TODOLIST_FILE_ERROR;
    }

    m_list_dump_binary(*list, database_file);
    fclose(database_file);

    return M_TODOLIST_OK;
}


m_todolist_error_code_t add_todo(m_list_t **list, m_com_sized_data_t *buffer)
{
    m_list_append_to_end_store(*list, buffer);
    return M_TODOLIST_OK;
}


m_todolist_error_code_t delete_todo(m_list_t **list, m_com_sized_data_t *buffer)
{
    m_list_delete_by_value(*list, buffer);
    return M_TODOLIST_OK;
}

m_todolist_error_code_t list_todos(m_list_t **list, m_com_sized_data_t *buffer)
{
    m_list_print(*list);
    return M_TODOLIST_OK;
}

m_todolist_error_code_t exit_todolist(m_list_t **list, m_com_sized_data_t *buffer)
{
    return M_TODOLIST_EXIT;
}

void add_command_to_map(m_map_t *map, char *command, m_todolist_error_code_t (*function)(m_list_t **list, m_com_sized_data_t *buffer))
{
    m_com_sized_data_t key, value;

    key.size = strlen(command);
    key.data = command;

    value.size = sizeof(function);
    value.data = function;

    m_map_set(map, &key, &value);
}
